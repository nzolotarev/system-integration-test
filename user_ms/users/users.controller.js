﻿const express = require('express');
const router = express.Router();
const userService = require('./user.service');
const config = require('config.json');
const mongoose = require('mongoose');

// routes
router.post('/authenticate', authenticate);
router.post('/register', register);
router.get('/', getAll);
router.get('/current', getCurrent);
router.get('/:id', getById);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function authenticate(req, res, next) {
    userService.authenticate(req.body, async ()=>{
        let username = req.body.username;
        let user = await require('_helpers/db').User.findOne({ username });
        return user;
    })
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    userService.getAll(require('_helpers/db').User)
        .then(users => res.json(users))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    userService.getById(req.user.sub, require('_helpers/db').User)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    userService.getById(req.params.id, require('_helpers/db').User)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function register(req, res, next) {
    mongoose.connect(process.env.MONGODB_URI || config.connectionString, { useCreateIndex: true, useNewUrlParser: true });
    mongoose.Promise = global.Promise;
    let connection = require('../users/user.model')
    userService.create(req.body, connection)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function update(req, res, next) {
    mongoose.connect(process.env.MONGODB_URI || config.connectionString, { useCreateIndex: true, useNewUrlParser: true });
    mongoose.Promise = global.Promise;
    let connection = require('../users/user.model')
    userService.update(req.params.id, req.body, connection)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    mongoose.connect(process.env.MONGODB_URI || config.connectionString, { useCreateIndex: true, useNewUrlParser: true });
    mongoose.Promise = global.Promise;
    let connection = require('../users/user.model')
    userService.delete(req.params.id, connection)
        .then(() => res.json({}))
        .catch(err => next(err));
}